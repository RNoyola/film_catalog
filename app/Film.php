<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
  public function genres(){
    return $this->belongsToMany(Genre::class);
  }

  public function posts(){
    return $this->hasMany(Post::class);
  }

}
