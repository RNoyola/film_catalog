<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;
use App\Genre;
use Illuminate\Support\Facades\Storage;
use Image;
class FilmController extends Controller
{
    public function index(){
      $films = Film::with('genres')->paginate(1);
      $posts = Film::where("id",$films[0]->id)->first()->posts()->with('user')->get();
      return view('films.main',['films' => $films,'posts'=>$posts]);
    }

    public function index_specific($name){
      $films = Film::with('genres')->where('slug_name',$name)->first();
      $posts = Film::where("id",$films->id)->first()->posts()->get();
      return view('films.search',['films' => $films,'posts'=>$posts]);
    }

    public function create(){
      $genres = Genre::get();
      return view('films.create',["genres"=>$genres]);
    }

    public function store(Request $request){
    //  dd($request) ;
        $validateData = $request->validate([
          'name' => 'required|max:100',
          'description' => 'required|max:255',
          'date'  =>  'required|date',
          'inlineRadioOptions' => 'required|digits_between:1,5',
          'price' =>  'required|numeric|min:0',
          'country' => 'required',
          'genre' => 'required',
          'photo' => 'required'
      ]);

      $film = new Film;
      $film->name = $request->name;
      $film->description = $request->description;
      $film->release_date = $request->date;
      $film->rating = $request->inlineRadioOptions;
      $film->ticket_price = $request->price;
      $film->country = $request->country;

      $image = $request->file('photo');
			$filename  = $film->name. '.' . $image->getClientOriginalExtension();
			$path = public_path('img/' . $filename);
			Image::make($image->getRealPath())->save($path);

      $film->photo_path = "img/".$filename;
      $invalidString = [" ",":","/","\\","\'","\""];
      $change=["-","-","","","",""];

      $film->slug_name = str_replace($invalidString, $change, $film->name);
      $film->save();
      //$film->name = $request->name;

      foreach (explode(',',$request->genre) as $key => $value) {
        if($value!=""){
          $g = Genre::where('name',$value)->first();
          if(!$g){
            $gen = new Genre;
            $gen->name = $value;
            $gen->save();
            $g = $gen;
          }
          $film->genres()->attach($g->id);
      }
    }

      $films = Film::with('genres')->paginate(1);
      $posts = Film::where("id",$films[0]->id)->first()->posts()->get();
      return view('films.main',['films' => $films,'posts'=>$posts]);

    }
}
