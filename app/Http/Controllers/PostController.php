<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Post;
class PostController extends Controller
{
    public function store(Request $request){
      $validateData = $request->validate([
        'comment' => 'required|min:1',
        'film'=> 'required|numeric'
      ]);
      $user = Auth::user();
      $post = new Post;
      $post->comment = $request->comment;
      $post->user_id = $user->id;
      $post->film_id = $request->film;
      $post->save();
      return back();
    }
}
