@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
              <form enctype="multipart/form-data" action="/films" method="POST">
                <div class="panel-heading">
                  <div class="title">
                    <h1>Create New Film:</h1>
                  </div>
                </div>
                <div class="panel-body">

                    {{csrf_field()}}
                    <label>Film Name:</label>
                    <input name="name" class="form-control" type="text" placeholder="Film Name"  value="{{ old('name') }}" required/>
                    <label>Description:</label>
                    <textarea name="description" class="form-control" placeholder="Description" value="{{ old('description') }}" required rows="3"/></textarea>
                    <label>Release Date:</label>
                    <input name="date" class="form-control" type="text" placeholder="yyyy/mm/dd" value="{{ old('date') }}" required/>
                    <label>Your Score:</label>
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1"> 1
                      </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2"> 2
                      </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="3"> 3
                      </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="4"> 4
                      </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio5" value="5" > 5
                      </label>
                    </div>
                    <label>Ticket Price:</label>
                    <input class="form-control" name="price" type="number" placeholder="Price" value="{{ old('price') }}" required/>
                    <label>Country:</label>
                    <input class="form-control" name="country" type="text" placeholder="Country Name" value="{{ old('country') }}" required/>
                     <label>Genres:</label>
                    <!-- -->

                    <div class="dropdown">
                      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Genres
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach($genres as $genre)
                          <a class="dropdown-item" href="#" value="{{$genre->name}}" id="genreButton">{{$genre->name}}</a>
                        @endforeach
                      </div>
                    </div>
                    <br>
                    <input id="inputGenres" class="form-control" name="genre" type="text" placeholder="Genres" value="{{ old('genre') }}" required readonly/>
                    <label>Upload Image:</label>
                    <input class="form-control" name="photo" type="file" placeholder="Picture" value="{{ old('photo') }}" accept=".png,.jpeg,.jpg" required />

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                </div>
                <div class="panel-footer" style="float:right">
                  <button  type="submit" class="btn btn-primary">Save</button>

                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){

  $("#genreButton").on( "click", function() {
    if(!$("#inputGenres").text().indexOf($(this).text()) >= 0)
      $("#inputGenres").val($(this).text()+$("#inputGenres").text()+',')
  });

});
</script>

@endsection
