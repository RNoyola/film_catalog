@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="title">
                    <h1>{{$films[0]->name}}<h1>
                  </div>
                  <div class="release">
                    {{$films[0]->release_date}}
                  </div>

                </div>
                <div class="panel-body">
                    <div class="poster">
                      <img src="{{$films[0]->photo_path}}"/>
                    </div>
                    <div class="general">
                    <p> Country: {{$films[0]->country}} | Ticket Price: ${{$films[0]->ticket_price}} | Rating: {{$films[0]->rating}} / 5  </p>
                    </div>
                    <div>
                      Genres
                      @foreach($films[0]->genres as $genre)
                        | {{ $genre->name }}
                      @endforeach
                    </div>
                    <div class="description">
                      {{$films[0]->description}}
                    </div>
                </div>
                <div class="panel-footer" >
                  <div class="pagination">
                    {{$films}}
                  </div>
                  <br>
                  <div class="comments">
                    <label>
                      Comments
                    </label>
                    @foreach($posts as $post)
                      <div style=" border: 0.5px solid black;box-sizing: border-box;border-radius: 5px;"><!-- It's kind of ugly but I need to start the next task -->
                          Username: {{$post->user->name}}
                          <br>
                        {{$post->comment}}
                      </div>
                      <br>
                    @endforeach
                    <div class="logged users Comment">
                    @if(Auth::check())
                      <div>
                        <form action="/posts" method="POST">
                          {{csrf_field()}}
                          <input name="film" value="{{$films[0]->id}}" hidden/>
                          <textarea name="comment" class="form-control" placeholder="Comment" required rows="3"/></textarea>
                          <br>
                          <button class="btn btn-primary">Post</button>
                        </form>
                      </div>
                    @endif
                  </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
