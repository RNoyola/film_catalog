@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="title">
                    <h1>{{$films->name}}<h1>
                  </div>
                  <div class="release">
                    {{$films->release_date}}
                  </div>

                </div>
                <div class="panel-body">
                    <div class="poster">
                      <img src="/{{$films->photo_path}}"/>
                    </div>
                    <div class="general">
                    <p> Country: {{$films->country}} | Ticket Price: ${{$films->ticket_price}} | Rating: {{$films->rating}} / 5  </p>
                    </div>
                    <div>
                      Genres
                      @foreach($films->genres as $genre)
                        | {{ $genre->name }}
                      @endforeach
                    </div>
                    <div class="description">
                      {{$films->description}}
                    </div>
                </div>

                <div class="panel-footer" >
                  <div class="comments">
                    <label>
                      Comments
                    </label>
                    @foreach($posts as $post)
                      <div style=" border: 0.5px solid black;box-sizing: border-box;border-radius: 5px;">
                          Username: {{$post->user->name}}
                          <br>
                        {{$post->comment}}
                      </div>
                      <br>
                    @endforeach
                    <div class="logged users Comment">
                    @if(Auth::check())
                      <div>
                        <form action="/posts" method="POST">
                          {{csrf_field()}}
                          <input name="film" value="{{$films->id}}" hidden/>
                          <textarea name="comment" class="form-control" placeholder="Comment" required rows="3"/></textarea>
                          <br>
                          <button class="btn btn-primary">Post</button>
                        </form>
                      </div>
                    @endif
                  </div>
                  </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
