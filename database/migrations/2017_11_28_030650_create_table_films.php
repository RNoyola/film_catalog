<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFilms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('films', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name',100);
          $table->string('description',255);
          $table->date('release_date');
          $table->enum('rating',[1,2,3,4,5]);
          $table->double('ticket_price', 8, 2);
          $table->string('country',50);
          $table->string('photo_path');
          $table->timestamps();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
