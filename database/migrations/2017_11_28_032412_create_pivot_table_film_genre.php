<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTableFilmGenre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('film_genre', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('film_id')->unsigned();
          $table->integer('genre_id')->unsigned();
          $table->timestamps();

          $table->foreign('film_id')->references('id')->on('films')->onDelete('cascade')->onUpdate('cascade');
          $table->foreign('genre_id')->references('id')->on('genres')->onDelete('cascade')->onUpdate('cascade');

      });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
