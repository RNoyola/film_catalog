<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class FilmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('films')->insert([
          'name' => 'Die Hard',
          'description' => 'John McClane, officer of the NYPD, tries to save his wife Holly Gennaro and several others that were taken hostage by German terrorist Hans Gruber during a Christmas party at the Nakatomi Plaza in Los Angeles.',
          'release_date' => Carbon::now(),
          'rating' => 4,
          'ticket_price' => 7.5,
          'country' => 'USA',
          'photo_path' => 'img/die_hard.png',
          'slug_name' => 'die-hard'
      ]);

      DB::table('films')->insert([
          'name' => 'Mad Max:Fury Road',
          'description' => 'Years after the collapse of civilization, the tyrannical Immortan Joe enslaves apocalypse survivors inside the desert fortress the Citadel.',
          'release_date' => Carbon::now(),
          'rating' => 3,
          'ticket_price' => 7.5,
          'country' => 'USA',
          'photo_path' => 'img/mad_max.png',
          'slug_name' => 'mad-max'
      ]);

      DB::table('films')->insert([
          'name' => 'The Terminator',
          'description' => 'Disguised as a human, a cyborg assassin known as a Terminator (Arnold Schwarzenegger) travels from 2029 to 1984 to kill Sarah Connor (Linda Hamilton).',
          'release_date' => Carbon::now(),
          'rating' => 3,
          'ticket_price' => 7.5,
          'country' => 'USA',
          'photo_path' => 'img/illbeback.png',
          'slug_name' => 'the-terminator'
      ]);
    }
}
