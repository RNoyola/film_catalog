<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('posts')->insert([
          'comment' => 'Awesome movie',
          'user_id' => DB::table('users')->select('id')->where('email','john_doe@gmail.com')->first()->id,
          'film_id' => DB::table('films')->select('id')->where('name','Die Hard')->first()->id
      ]);
      DB::table('posts')->insert([
          'comment' => 'Awesome movie',
          'user_id' => DB::table('users')->select('id')->where('email','john_doe@gmail.com')->first()->id,
          'film_id' => DB::table('films')->select('id')->where('name','Mad Max:Fury Road')->first()->id
      ]);
      DB::table('posts')->insert([
          'comment' => 'Awesome movie',
          'user_id' => DB::table('users')->select('id')->where('email','john_doe@gmail.com')->first()->id,
          'film_id' => DB::table('films')->select('id')->where('name','The Terminator')->first()->id
      ]);
    }
}
