<?php

use Illuminate\Database\Seeder;


class FilmGenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('film_genre')->insert([
          'film_id' => DB::table('films')->select('id')->where('name','Die Hard')->first()->id,
          'genre_id' => DB::table('genres')->select('id')->where('name','Action')->first()->id
      ]);

      DB::table('film_genre')->insert([
          'film_id' => DB::table('films')->select('id')->where('name','Mad Max:Fury Road')->first()->id,
          'genre_id' => DB::table('genres')->select('id')->where('name','Action')->first()->id
      ]);

      DB::table('film_genre')->insert([
          'film_id' => DB::table('films')->select('id')->where('name','The Terminator')->first()->id,
          'genre_id' =>DB::table('genres')->select('id')->where('name','Action')->first()->id
      ]);


    }
}
