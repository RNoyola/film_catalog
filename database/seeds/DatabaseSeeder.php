<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()//CALL ALL OUR SEEDS TO FILL DB WITH 1 USER, 1 GENRE, 3 MOVIES AND 1 COMMENT FOR MOVIE
    {
      $this->call([
        UserTableSeeder::class,
        GenreTableSeeder::class,
        FilmTableSeeder::class,
        FilmGenreTableSeeder::class,
        PostTableSeeder::class,
      ]);
        // $this->call(UsersTableSeeder::class);
    }
}
